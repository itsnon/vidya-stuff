# Non's Quick and Dirty Modded Minecraft Setup
## _(For Curseforge Mods)_

### 1. **Buy Minecraft/Setup Account**
Go [here](https://www.minecraft.net/en-us/) and buy Minecraft if you don't have it already _(Make sure to get the Java edition - the Win10 edition is cool and has cross-play and stuff but the mod support is nowhere near as robust)_ - if you haven't played in a long time, upgrade to the new username/password system [here](https://account.mojang.com/migrate) - once done, login, set your name, skin, etc.

> **NOTE** - If you want a fancy custom skin, head to [Minecraft Skins](https://www.minecraftskins.com/search/skin/player/1/) and find one you like. There's an `Upload to Minecraft.net` button right on the skin page.

### 2. **Install Java Runtimes**
Easiest way is probably just to grab an [OpenJDK](https://adoptopenjdk.net/) build so you don't have to fuck with Oracle shit. You'll want version 8.

### 3. **Download and Setup MultiMC**
[MultiMC](https://multimc.org/) allows you to have multiple managed versions of Minecraft installed at once with different modsets.

When you first run it, it'll ask you to pick a Java version. Any 8 runtime should be fine. (If you just installed the JDK above, you'll likely need to reboot for the environment variable to be picked up)

> **NOTE** - You need to change the JVM memory settings below or you won't be able to load the modpacks without crashing

You'll want to head into `Settings` on the menu bar and go into the `Java` section - you'll want to raise the Minimum and Maximum memory allocation for the JVM, I went with 4096 MB and 8192 MB respectively and I haven't had issues.

Additionally, head into the `Accounts` section and plugin your Minecraft username and password. This way, the client will auto log you in, grab builds, and so on.

### 4. **Grab a Modpack From Curseforge and Add to MultiMC**

Head to the [Curseforge Modpack list](https://www.curseforge.com/minecraft/modpacks). From there, you'll want to pick the mod you want to play and then click the Files tab at the top. Click into the most recent version that isn't the Server Pack, and you'll see a Download button.

> **NOTE** - If it just shows the Twitch Client Install button, you need to actually click into the file's page to see the download button.

Once the zip file is downloaded, head back into MultiMC. Click the `Add Instance` button on the top. On the left side, pick the `Import from zip` option. Click `Browse` and point it at the zip file you just downloaded. Give it a name, and click OK. It will grab Minecraft, download all the required mods, and install the modpack.

From there, if you're only playing Singleplayer, that's all you need to do - go ahead and launch the new icon from MultiMC and you're all set. For Multiplayer, though, go ahead and do step 5.

### 5. **Get Whitelisted on the Server**

Since I'll be running a whitelist on the server, you'll want to make sure I have your UUID to whitelist you. Go ahead and visit [MCUUID](https://mcuuid.net/), plug in your playername, and get me both your playername + UUID. Once I have these, I can plug them in and whitelist you for server access.

Once you're whitelisted, you'll launch the game with the appropriate modpack. Click the Multiplayer option, click Add Server, and plugin the address you were given. The server will show up on the list, and you're good to go!

## Bonus Customization Steps

### **Resource Packs**

Resource packs change the look of all the textures in Minecraft. After an instance has been created in MultiMC, you can right-click it, choose `Minecraft Folder`, and then open up the `resourcepacks` folder and drop a resource pack zip file in there. Once done, you will be able to select it ingame.

The creator of RLcraft recommends [Chroma Hills](https://www.chromahills.com/) - make sure you grab the zip file that corresponds to the version of Minecraft you're on. (Most of the Curseforge mods are built on 1.12.2)

I am personally a fan of the [Faithful pack](https://faithful.team/downloads/), which is basically a higher detail version of the standard textures.

If you want browse for your own, you can go to [resourcepack.net](https://resourcepack.net/) and grab a pack that matches your Minecraft version.

### **OptiFine and Shader Packs**

[OptiFine](https://optifine.net/) is a mod for Minecraft that adds a ton of overhaul to the Minecraft render engine, as well as adds things like custom shader support and support for super hi-rez textures above 128x128. For copyright reasons, modpacks can't include it in their releases, but it's compatible with a grand majority of them, if not all of them.

Simply go to the [OptiFine download page](https://optifine.net/downloads) and grab the appropriate version for the Minecraft version your mod runs on. Once that's done, go to MultiMC and right-click the mod instance and choose `Minecraft Folder`. Once that opens, drop the OptiFine jar file you downloaded into the `mods` folder. Since Curseforge/FTB mods all use Forge, that should load just fine and you'll see all sorts of additional options in the video settings menu.

To load in custom shaders, create a folder called `shaderpacks` in your Minecraft folder for your mod instance. Once that's done, much like resource packs, you can simply drop the zip file in there.

To change shaders, once ingame, the Video settings option now has a `Shaders...` button that lists all the shaders in the `shaderpacks` folder.

The site [Shaders Mods](http://shadersmods.com/category/shaderpacks/) has a lot of options for shader packs. The [Silphur's Shaders](http://shadersmods.com/sildurs-shaders-mod/) pack has some great options, though I will recommend you get a pack that works with your system, since shaders can have a giant impact on your ingame performance.

I'm a fan of [SEUS Renewed](https://www.sonicether.com/seus/), which is pretty good and you can tweak a lot of the settings to your liking.