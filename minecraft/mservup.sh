#!/bin/bash

MCENV=$1

if [[ -z "${MCENV}" ]]; then
    echo "MCENV not declared"
    exit 1
fi

docker-compose -f /opt/minecraft/dc/${MCENV}/docker-compose.yml up -d
